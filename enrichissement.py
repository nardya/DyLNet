#!/usr/bin/python
# -*- coding: UTF-8 -*-

from normalisation import normalize
from nettoyage import nettoye
from tagging import tag
import os
import re
import xml.etree.ElementTree as ET
import copy



""" enrichissement(name)
    Cette fonction permet, à partir d'un fichier _VA.eaf, de creer
    un fichier _VB.eaf avec les lignes de normalisation, de nettoyage
    et de tagging ajoutés

    Entrée:
        name: Chemin d'accès au fichier à traiter (String)
    Sorties:
        fichier_errone: Variable indiquant si le fichier comporte une
			annotation vide ou non (Boolean)
		Création d'un fichier _VB.eaf enrichi
"""
def enrichissement (name):
	#On vérifie que le fichier est bien un fichier _VA.eaf
	if re.search("_VA",name):
		filename=name
		#On parcourt le fichier
		originaltree = ET.parse(filename,ET.XMLParser(encoding='utf-8'))
		#On copie l'arbre original afin de ne pas modifier le fichier d'origine
		tree=copy.deepcopy(originaltree)
		#On lance les trois scripts d'enrichissement successivement
		tree,fichier_errone=normalize(tree)
		tree=nettoye(tree)
		tree=tag(tree)
		#Si nécessaire, on crée les dossiers de sortie
		if not os.path.exists('./Sortie'):
			os.mkdir('./Sortie')
		if not os.path.exists('./Sortie/Enrichis'):
			os.mkdir('./Sortie/Enrichis')
		#On crée le nom du nouveau fichier
		new_filename=re.search('([^\/.]*?.eaf)',filename)
		new_filename=new_filename.group(1)
		new_filename=re.sub('_VA','_VB',new_filename)
		new_filename="Sortie/Enrichis/"+new_filename
		#On enregistre le nouveau fichier
		tree.write(new_filename,encoding="utf-8")
		return fichier_errone
